with (import <nixpkgs> {});
stdenv.mkDerivation rec {
   name = "acorn-ca";
   buildInputs = [
     gcc
     gnumake
     SDL2
   ];
}
